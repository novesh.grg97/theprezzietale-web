import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Information from './components/Information';
import Testimonial from './components/Testimonial';
import GridView from './components/GridView';
import Content from './components/Content';
class App extends Component {
	constructor() {
		super();
		this.state = {};
	}
	render() {
		return (
			<div className="app">
				<Header />
				<div className="app-content">
					<Information />
					<Content />
					<Testimonial />
					<GridView />
					<Footer />
				</div>
			</div>
		);
	}
}

export default App;
