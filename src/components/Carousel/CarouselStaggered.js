import React from "react";

import PropTypes from "prop-types";

import classes from "./Carousel.module.css";
import leftScroll from "./assets/leftScroll.svg";
import leftScrollDisabled from "./assets/left-scroll-disabled.svg";
import rightScroll from "./assets/rightScroll.svg";
import rightScrollDisabled from "./assets/right-scroll-disabled.svg";

class CarouselStaggered extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeSlide: this.props.activeSlide
    };
    this.ul_Ref = React.createRef();
    this.imgDownloadAnchors = [];
  }

  moveLeft = evt => {
    evt && evt.stopPropagation();
    this.setState(({ activeSlide }) => {
      return { activeSlide: activeSlide ? activeSlide - 1 : 0 };
    });
  };

  moveRight = evt => {
    evt && evt.stopPropagation();
    if (this.state.activeSlide >= this.state.slides - 1) return;
    this.setState(({ activeSlide }) => {
      const length = this.props.elements.length;
      return {
        activeSlide:
          activeSlide + this._windowLength >= length
            ? activeSlide
            : activeSlide + 1
      };
    });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.elements !== this.props.elements) {
      this.setingWidth();
    }
    if (
      prevProps.activeSlide != this.props.activeSlide &&
      this.props.activeSlide > this.state.activeSlide + this._windowLength - 1
    ) {
      this.moveRight();
    }
    if (
      prevProps.activeSlide != this.props.activeSlide &&
      this.props.activeSlide < this.state.activeSlide
    ) {
      this.moveLeft();
    }
  }

  download = e => {
    e.preventDefault();
    var link = document.createElement("a");

    link.style.display = "none";
    link.setAttribute("target", "_blankl");

    document.body.appendChild(link);

    for (var i = 0; i < this.props.elements.length; i++) {
      // link.setAttribute("href", this.props.elements[i]);
      // link.click();
      window.open(this.props.elements[i]);
    }

    document.body.removeChild(link);
  };
  returningDots = () => {
    let arr = [];
    for (let i = 0; i < this.state.slides; i++) {
      arr.push(
        <span
          key={i}
          onClick={() => {
            this.setState({
              activeSlide: i
            });
          }}
          className={
            classes.dots +
            " " +
            (this.state.activeSlide == i ? classes.activeDot : "")
          }
        >
          &nbsp;
        </span>
      );
    }
    return arr;
  };
  setingWidth = () => {
    let ulElement = this.ul_Ref.current;
    let clientWidth = ulElement.parentElement.getBoundingClientRect().width;
    let newWidth = 0,
      scrollWidth = 0;

    for (let i = 0; i < ulElement.children.length; i++) {
      scrollWidth += ulElement.children[i].getBoundingClientRect().width;
    }

    let halfScrollWidth = Math.ceil(scrollWidth / 2);

    if (scrollWidth > clientWidth) {
      for (let i = 0; i < ulElement.children.length; i++) {
        newWidth += ulElement.children[i].getBoundingClientRect().width;
        if (newWidth >= halfScrollWidth) {
          break;
        }
      }
    }

    newWidth = Math.max(newWidth, clientWidth);

    ulElement.style.width = `${newWidth}px`;
    ulElement.style.flexWrap = "wrap";
    let translateBy = clientWidth / 2;
    let slides = Math.floor(newWidth / translateBy);
    let maxTranslate = ulElement.scrollWidth - clientWidth;

    if (scrollWidth <= clientWidth) {
      slides = 0;
    }

    this.setState({
      translateBy,
      slides,
      maxTranslate
    });
    this._windowLength =
      2 * parseInt(this._container.clientWidth / this.props.elementWidth) - 1;
  };
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        showShimmer: true
      });
    }, 3000);
  }

  render() {
    let { elements, width, elementWidth } = this.props;
    let { activeSlide } = this.state;
    return (
      <div
        style={{
          marginTop: "2%",
          width: "100%"
          //width: "90%"
          // overflow: "hidden"
        }}
      >
        <div
          className={classes.Container}
          style={{ width }}
          ref={elm => (this._container = elm)}
        >
          {this.props.head && (
            <div className={classes.carouselHeader}>{this.props.head}</div>
          )}
          <div
            style={{
              overflow: "hidden"
            }}
          >
            <ul
              ref={this.ul_Ref}
              style={{
                transform: `translate(${-1 *
                  Math.min(
                    activeSlide * this.state.translateBy,
                    this.state.maxTranslate
                  )}px)`,
                display: "flex"
              }}
            >
              {elements.map((elem, i) => (
                <li key={elem + i} style={{ background: "transparent" }}>
                  {elem}
                </li>
              ))}
            </ul>
          </div>
          {this.state.slides > 1 && !this.props.loading && (
            <>
              {!(activeSlide == 0) && (
                <button className={classes.Left}>
                  <span
                    className={
                      classes.roundButton +
                      " " +
                      (activeSlide == 0 ? classes.disabledRound : "")
                    }
                    onClick={this.moveLeft}
                  >
                    <img
                      style={{
                        transform: "rotate(180deg)",
                        marginRight: "2px"
                      }}
                      src={activeSlide > 0 ? rightScroll : rightScrollDisabled}
                      alt=""
                    />
                  </span>
                </button>
              )}
              {!(this.state.activeSlide >= this.state.slides - 1) && (
                <button className={classes.Right}>
                  <span
                    className={
                      classes.roundButton +
                      " " +
                      (this.state.activeSlide >= this.state.slides - 1
                        ? classes.disabledRound
                        : "")
                    }
                    onClick={this.moveRight}
                  >
                    <img
                      src={
                        this.state.activeSlide >= this.state.slides - 1
                          ? rightScrollDisabled
                          : rightScroll
                      }
                      alt=""
                    />
                  </span>
                </button>
              )}
            </>
          )}
        </div>
        {this.state.slides > 1 && this.state.slides < 20 && !this.props.loading && (
          <div className={classes.dotNavigations}>{this.returningDots()}</div>
        )}
      </div>
    );
  }
}

CarouselStaggered.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.element).isRequired,
  style: PropTypes.object,
  showButtons: PropTypes.bool
};

export default CarouselStaggered;
