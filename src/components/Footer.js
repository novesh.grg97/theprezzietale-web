import React, { Component } from 'react';
import '../css/landing.css';
import gmail from '../images/gmail.svg';
import facebook from '../images/facebook.svg';
import instagram from '../images/insta-white.svg';
import phone from '../images/phone.svg';
import logo from '../images/theprezzietale-images/logo-details/headerLogoGreen.png';
class Footer extends Component {
	constructor() {
		super();
		this.state = {};
	}
	render() {
		return (
			<div className="footer">
				<b>
					<div className="footer-content ">
						<div className="footer-content-item1">
							<img src={logo} alt="logo" style={{ width: '200px', height: '80px' }} />
						</div>
						<div className="footer-content-item2">
							<p className="footer-content-itemHeading1">CONTACT INFO</p>
							<div className="footer-contentGrid">
								<div className="footer-content-list1">
									<p className="footer-content-itemText">Gunjan Garg (Founder)</p>
									<p className="footer-content-itemText">
										<img src={phone} alt="" /> +91-9990083282
									</p>
									<p className="footer-content-itemText">
										<a href="mailto:gunjan@theprezzietale.com" target="_blank">
											<img src={gmail} alt="gmail" style={{ marginRight: '8px' }} />{' '}
											gunjan@theprezzietale.com
										</a>
									</p>
								</div>
								<div className="footer-content-list2">
									<p className="footer-content-itemText">Novesh Garg (Co-Founder)</p>
									<p className="footer-content-itemText">
										<img src={phone} alt="phone" /> +91-9250904750
									</p>
									<p className="footer-content-itemText">
										<a href="mailto:novesh@theprezzietale.com" target="_blank">
											<img src={gmail} alt="gmail" style={{ marginRight: '8px' }} />{' '}
											novesh@theprezzietale.com
										</a>
									</p>
								</div>
							</div>
						</div>
						<div className="footer-content-item3">
							<p className="footer-content-itemHeading2">KEEP IN TOUCH</p>
							<div className="footer-content-image">
								<a href="mailto:novesh@theprezzietale.com" target="_blank">
									<img src={gmail} alt="gmail" style={{ width: '40px', height: '40px' }} />
								</a>
								<a href="https://www.facebook.com/theprezzietale" target="_blank">
									<img src={facebook} alt="facebook" style={{ width: '35px', height: '40px' }} />
								</a>
								<a href="https://www.instagram.com/the_prezzie_tale/" target="_blank">
									<img src={instagram} alt="instagram" style={{ width: '45px', height: '45px' }} />
								</a>
							</div>
						</div>
					</div>
					<div className="footer-copyright">
						© 2020 The Prezzie Tale <br />All Rights Reserved<br /> CIN : DL-10-0003484
					</div>
				</b>
			</div>
		);
	}
}

export default Footer;
