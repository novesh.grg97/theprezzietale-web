import React, { Component } from 'react';
import '../css/landing.css';
import logo from '../images/theprezzietale-images/logo-details/headerLogoGreen.png';

class Header extends Component {
	constructor() {
		super();
		this.state = {};
	}

	handleClick = () => {
		window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
	};
	render() {
		return (
			<div className="header">
				<img src={logo} alt="logo" className="header-logo" />
				<button className="header-button" onClick={this.handleClick}>
					Contact Us
				</button>
			</div>
		);
	}
}

export default Header;
