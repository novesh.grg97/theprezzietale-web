import React, { Component } from 'react';
import '../css/landing.css';

import h1 from '../images/theprezzietale-images/hampers/min/h1.jpeg';
import h2 from '../images/theprezzietale-images/hampers/min/h2.jpeg';
import h3 from '../images/theprezzietale-images/hampers/min/h3.jpeg';
import h4 from '../images/theprezzietale-images/hampers/min/h4.jpeg';
import h5 from '../images/theprezzietale-images/hampers/min/h5.jpeg';
import h6 from '../images/theprezzietale-images/hampers/min/h6.jpeg';
import h7 from '../images/theprezzietale-images/hampers/min/h7.jpeg';
import h8 from '../images/theprezzietale-images/hampers/min/h8.jpeg';
import h9 from '../images/theprezzietale-images/hampers/min/h9.jpeg';
import h10 from '../images/theprezzietale-images/hampers/min/h10.jpeg';
import h11 from '../images/theprezzietale-images/hampers/min/h11.jpeg';
import h12 from '../images/theprezzietale-images/hampers/min/h12.jpeg';

import h13 from '../images/theprezzietale-images/hampers/min/h13.jpeg';
import h14 from '../images/theprezzietale-images/hampers/min/h14.jpeg';
import h15 from '../images/theprezzietale-images/hampers/min/h15.jpeg';
import h16 from '../images/theprezzietale-images/hampers/min/h16.jpeg';
import h17 from '../images/theprezzietale-images/hampers/min/h17.jpeg';
import h18 from '../images/theprezzietale-images/hampers/min/h18.jpeg';
import h19 from '../images/theprezzietale-images/hampers/min/h19.jpeg';
import h20 from '../images/theprezzietale-images/hampers/min/h20.jpeg';
import h21 from '../images/theprezzietale-images/hampers/min/h21.jpeg';
import h22 from '../images/theprezzietale-images/hampers/min/h22.jpeg';
import h23 from '../images/theprezzietale-images/hampers/min/h23.jpeg';
import h24 from '../images/theprezzietale-images/hampers/min/h24.jpeg';


import insta from '../images/insta-white.svg';

class GridView extends Component {
	constructor() {
		super();
		this.state = {
			data: [
				{
					image: h13,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h14,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h15,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h16,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h5,
					link: 'https://www.instagram.com/p/CFE7rGUp4J5/?utm_source=ig_web_copy_link'
				},
				{
					image: h17,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h18,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h19,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h20,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h21,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h22,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h23,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h24,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h1,
					link: 'https://www.instagram.com/p/CDGjvxapo7t/?utm_source=ig_web_copy_link'
				},
				{
					image: h2,
					link: 'https://www.instagram.com/p/CD628CbJRCe/?utm_source=ig_web_copy_link'
				},
				{
					image: h3,
					link: 'https://www.instagram.com/p/CBDFJS1AZ3K/?utm_source=ig_web_copy_link'
				},
				{
					image: h4,
					link: 'https://www.instagram.com/p/CEdbpUOJku3/?utm_source=ig_web_copy_link'
				},
				{
					image: h6,
					link: 'https://www.instagram.com/p/CDAwb_8pP0Z/?utm_source=ig_web_copy_link'
				},
				{
					image: h7,
					link: 'https://www.instagram.com/p/CEdbUtypNSo/?utm_source=ig_web_copy_link'
				},
				{
					image: h8,
					link: 'https://www.instagram.com/oon.bunn/'
				},
				{
					image: h9,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h10,
					link: 'https://www.instagram.com/p/CBDFHAfgnsF/?utm_source=ig_web_copy_link'
				},
				{
					image: h11,
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					image: h12,
					link: 'https://www.instagram.com/thefernwehcollective/'
				}
			]
		};
	}

	handleClick = () => {
		window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
	};
	render() {
		let { data } = this.state;
		return (
			<div className="gridview">
				<p className="gridview-heading">La Galerie</p>

				<div className="gridview-wrapper">
					{data.map((dt, index) => {
						return (
							<div className="gridview-container">
								<img src={dt.image} className="gridview-image" alt="" />
								<div class="gridview-overlay">
									{/* <div class="gridview-text">Hello World</div> */}
									<a
										class="gridview-text"
										target="_blank"
										href={dt.link}
									>
										<img src={insta} alt="insta" style={{ width: '40px', height: '40px' }} />
									</a>
								</div>
							</div>
						);
					})}
				</div>
			</div>
		);
	}
}

export default GridView;
