import React, { Component } from 'react';
import '../css/landing.css';
import logo from '../images/theprezzietale-images/logo-details/headerLogoGreen.png';
import corporateCatalogue from '../document/corporate.pdf';

class Content extends Component {
	constructor() {
		super();
		this.state = {};
	}
	handleClick = () => {
		window.open(corporateCatalogue, '_blank');
	};

	render() {
		return (
			<div className="content-container">
				<p className="content-heading">Lets not make this Festive Season under Quarantine</p>
				<p className="content-subheading">After all the plans overshadowed by lockdowns, to our loved ones</p>
				<p className="content-subheading">lets celebrate this Diwali by sending a token of Love and Affection</p>
				<p className="content-subheading">while staying safe and healthy at the comfort of your bedside. </p>
				
				<p className="content-subheading">Delivering package of Love across India</p>
				<p className="content-subheading">Hurry Up! Book yours now :) </p>
				<button className="content-button" onClick={this.handleClick}>
					Diwali Catalogue
				</button>
			</div>
		);
	}
}

export default Content;
