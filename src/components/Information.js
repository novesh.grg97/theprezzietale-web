import React, { Component } from 'react';
import '../css/landing.css';
class Information extends Component {
	constructor() {
		super();
		this.state = {};
	}
	render() {
		return (
			<div className="information">
				<div className="information-container">
					<p className="information-heading"></p>
					<p className="information-text">SEALED WITH LOVE AND PATIENCE</p>
				</div>
			</div>
		);
	}
}

export default Information;
