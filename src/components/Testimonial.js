import React, { Component } from 'react';
import '../css/landing.css';
import Carousel from './Carousel/Carousel';

import h1 from '../images/theprezzietale-images/hampers/min/h13.jpeg';
import h2 from '../images/theprezzietale-images/hampers/min/h18.jpeg';
import h3 from '../images/theprezzietale-images/hampers/min/h19.jpeg';
import h4 from '../images/theprezzietale-images/hampers/min/h21.jpeg';
import h5 from '../images/theprezzietale-images/hampers/min/h17.jpeg';

import insta from '../images/insta-white.svg';
class ScheduledLive extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [
				{
					name: 'AMBER',
					image: h1,
					text: `Contents :
							• SILK POTLI WITH GOTTA WORK
							• HANDCRAFTED FLOWER BROOCH
							• FRUIT LUSH CANDIES JAR (FROM BAKINGTON)
							• SCENTED TEALIGHT CANDLE PACK OF 10 - 1 no.
							• HANDMADE TEALIGHT CANDLE HOLDERS - 2 nos.
							• DIWALI GREETING`,
					cost: 'Price : ₹ 250/-',
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					name: 'EMERALD',
					image: h2,
					text: `Contents :
							• WOODEN BASKET - SMALL
							• FRUIT LUSH CANDIES JAR (FROM BAKINGTON)
							• STONE CHOCOLATES JAR
							• SCENTED TEALIGHT CANDLE PACK OF 10 - 1 no.
							• HANDMADE TEALIGHT CANDLE HOLDER - 2 nos.
							• DIWALI GREETING`,
					cost: 'Price : ₹ 500/-',
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					name: 'CORAL ',
					image: h3,
					text: `Contents :
							• LEATHERETTE BASKET - SMALL
							• ROASTED & SALTED CASHEWS JAR
							• ROASTED & SALTED ALMONDS JAR
							• HANDI CANDLES SET OF 2
							• DIWALI GREETING`,
					cost: 'Price : ₹ 700/-',
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					name: 'SAPPHIRE',
					image: h4,
					text: `Contents :
							• LEATHERETTE TRAY (8X8)
							• ROASTED & SALTED CASHEWS JAR
							• ROASTED & SALTED ALMONDS JAR
							• CHOCO-WALNUT CAKE (FROM DOUGH & CREAM/BAKINGTON)
							• SCENTED TEALIGHT CANDLE PACK OF 10 - 1 no.
							• HANDI CANDLES SET OF 2
							• GEL CANDLES SET OF 2
							• METAL SHADOW TEALIGHT CANDLE HOLDER - 1 no.
							• DIWALI GREETING`,
					cost: 'Price : ₹ 1000/-',
					link: 'https://www.instagram.com/the_prezzie_tale/'
				},
				{
					name: 'AMETHYST',
					image: h5,
					text: `Contents :
							• WOODEN TRAY & JAR SET WITH RESIN EMBOSSED DESIGN
							• PREMIUM CASHEWS POTLI (250GM.)
							• PREMIUM ALMONDS POTLI (250GM.)
							• HANDMADE TEALIGHT HOLDER SET OF 2
							• TEALIGHT CANDLES (2)`,
					cost: 'Price : ₹ 1800/-',
					link: 'https://www.instagram.com/the_prezzie_tale/'
				}
			]
		};
	}
	render() {
		let { data } = this.state;
		return (
			<div className="testimonial">
				<Carousel
					elements={data.map((dt) => (
						// <div className="testimonial-card">

						<div className="testimonial-box">
							<p className="testimonial-heading">{dt.name}</p>
							<div className="testimonial-card">
								<div className="testimonial-image-div">
									<div className="testimonial-image-container">
										<img src={dt.image} alt="" className="testimonial-image" />
										<div class="testimonial-overlay">
											{/* <div class="gridview-text">Hello World</div> */}
											<a class="testimonial-overlay-text" target="_blank" href={dt.link}>
												<img
													src={insta}
													alt="insta"
													style={{ width: '40px', height: '40px' }}
												/>
											</a>
										</div>
									</div>
									<p className="testimonial-image-cost">{dt.cost}</p>
								</div>
								<p className="testimonial-text">{dt.text}</p>
							</div>
						</div>
					))}
					dotsDisabled={false}
					arrowDisabled={false}
					width="100%"
					elementWidth={window.screen.width}
					activeSlide={0}
					head={
						<div className="testimonial-major-div">
							<div className="testimonial-major-heading">Special Gifts for a Special Ocassion</div>
							<div className="testimonial-major-subheading">Lets Welcome Diwali 2020 together</div>
						</div>
					}
				/>
			</div>
		);
	}
}

export default ScheduledLive;
